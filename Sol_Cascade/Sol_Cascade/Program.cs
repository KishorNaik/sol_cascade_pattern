﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Cascade
{
    class Program
    {
        static void Main(string[] args)
        {
            Mail mailObj = new Mail();

            mailObj
                .UserName("kishor")
                .Password("123")
                .To("kishor.naik011.net@gmail.com")
                .Cc("navnath.memane@gmail.com")
                .Subject("Hi")
                .Body("Hi")
                .Attachment("D:\\Test.png")
                .Send();


            Mails mailsObj = new Mails(new MailEntity()
            {
                UserName = "kishor",
                Password = "123",
                To = "kishor.naik011.net@gmail.com",
                Cc = "navnath.memane@gmail.com",
                Subject = "hi",
                Body = "hi",
                Attachment = "D:\\Test.png"
            });
            mailsObj.Send();
                

        }
    }

    public class Mail
    {
        #region Declaration
        

        private string userName = null;
        private string password = null;
        private string smtpName = null;

        private String to = null;
        private string cc = null;

        private string subject = null;
        private string body = null;
        private string attachment = null;
        #endregion

        #region Public Method 
        public Mail UserName(string userName)
        {
            this.userName = userName;
            return this;
        }
        public Mail Password(string password)
        {
            this.password = password;
            return this;
        }
        public Mail SmtpName(string smtpName)
        {
            this.smtpName = smtpName;
            return this;
        }

        public Mail To(string to)
        {
            this.to = to;
            return this;
        }

        public Mail Cc(string cc)
        {
            this.cc = cc;
            return this;
        }

        public Mail Subject(string subject)
        {
            this.subject = subject;

            return this;
        }

        public Mail Body(string body)
        {
            this.body = body;

            return this;
        }

        public Mail Attachment(string attachment)
        {
            this.attachment = attachment;

            return this;
        }

        public void Send()
        {
            System.Console.WriteLine(this.to);
            System.Console.WriteLine(this.subject);
            System.Console.WriteLine(this.body);
            System.Console.WriteLine("Mail Send");
        }
        #endregion 
    }

    public class MailEntity
    {
        public String UserName { get; set; }

        public String Password { get; set; }

        public String SmptName { get; set; }

        public String To { get; set; }

        public String Cc { get; set; }

        public String Subject { get; set; }

        public String Body { get; set; }

        public String Attachment { get; set; }
    }

    public class Mails
    {
        #region Declaration
        private MailEntity mailEntityObj = null;
        #endregion

        #region Constructor
        public Mails(MailEntity mailEntityObj)
        {
            this.mailEntityObj = mailEntityObj;
        }

        public void Send()
        {
            System.Console.WriteLine(mailEntityObj.To);
            System.Console.WriteLine(mailEntityObj.Cc);
            System.Console.WriteLine(mailEntityObj.Subject);
            System.Console.WriteLine(mailEntityObj.Body);
            System.Console.WriteLine("Mail Send");
        }

        #endregion 
    }
}
